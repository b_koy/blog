'use strict';

blogApp.factory('Subcomment', function ($resource) {
        return $resource('app/rest/subcomments/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': { method: 'GET'}
        });
    });
