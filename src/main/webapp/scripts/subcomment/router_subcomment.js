'use strict';

blogApp
    .config(function ($routeProvider, $httpProvider, $translateProvider, USER_ROLES) {
            $routeProvider
                .when('/subcomment', {
                    templateUrl: 'views/subcomments.html',
                    controller: 'SubcommentController',
                    resolve:{
                        resolvedSubcomment: ['Subcomment', function (Subcomment) {
                            return Subcomment.query();
                        }],
                        resolvedComment: ['Comment', function (Comment) {
                            return Comment.query();
                        }]
                    },
                    access: {
                        authorizedRoles: [USER_ROLES.all]
                    }
                })
        });
