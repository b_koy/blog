'use strict';

blogApp.controller('SubcommentController', function ($scope, resolvedSubcomment, Subcomment, resolvedComment) {

        $scope.subcomments = resolvedSubcomment;
        $scope.comments = resolvedComment;

        $scope.create = function () {
            Subcomment.save($scope.subcomment,
                function () {
                    $scope.subcomments = Subcomment.query();
                    $('#saveSubcommentModal').modal('hide');
                    $scope.clear();
                });
        };

        $scope.update = function (id) {
            $scope.subcomment = Subcomment.get({id: id});
            $('#saveSubcommentModal').modal('show');
        };

        $scope.delete = function (id) {
            Subcomment.delete({id: id},
                function () {
                    $scope.subcomments = Subcomment.query();
                });
        };

        $scope.clear = function () {
            $scope.subcomment = {text: null, author: null, create_date: null, change_date: null, id: null};
        };
    });
