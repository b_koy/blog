'use strict';

blogApp
    .config(function ($routeProvider, $httpProvider, $translateProvider, USER_ROLES) {
            $routeProvider
                .when('/post', {
                    templateUrl: 'views/posts.html',
                    controller: 'PostController',
                    resolve:{
                        resolvedPost: ['Post', function (Post) {
                            return Post.query();
                        }],
                        resolvedComment: ['Comment', function (Comment) {
                            return Comment.query();
                        }]
                    },
                    access: {
                        authorizedRoles: [USER_ROLES.all]
                    }
                })
        });
