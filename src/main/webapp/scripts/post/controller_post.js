'use strict';

blogApp.controller('PostController', function ($scope, resolvedPost, Post, resolvedComment) {

        $scope.posts = resolvedPost;
        $scope.comments = resolvedComment;

        $scope.create = function () {
            Post.save($scope.post,
                function () {
                    $scope.posts = Post.query();
                    $('#savePostModal').modal('hide');
                    $scope.clear();
                });
        };

        $scope.update = function (id) {
            $scope.post = Post.get({id: id});
            $('#savePostModal').modal('show');
        };

        $scope.delete = function (id) {
            Post.delete({id: id},
                function () {
                    $scope.posts = Post.query();
                });
        };

        $scope.clear = function () {
            $scope.post = {title: null, text: null, create_date: null, change_date: null, id: null};
        };
    });
