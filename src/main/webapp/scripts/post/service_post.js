'use strict';

blogApp.factory('Post', function ($resource) {
        return $resource('app/rest/posts/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': { method: 'GET'}
        });
    });
