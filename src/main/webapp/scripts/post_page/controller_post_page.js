'use strict';
blogApp.controller('PostPageController', function ($scope, $http, $routeParams, $q, LoadTagsService) {
    $scope.TECH_POSTS_URL = '/#/tech_posts';
    $scope.id = null;
    $http.get("/app/rest/post_page/" + $routeParams.id)
        .success(function (Post) {
            $scope.post = Post;
            //Set SEO
            angular.element($("[name='description']")).attr('content', $scope.post.seoDescription);
            angular.element($("[name='keywords']")).attr('content',$scope.post.seoKeywords);

        }).error(function (data) {
            alert(data.message);
        });

    $.ajax({
            url: "/app/rest/tech_posts/getPostTypes",
            dataType: 'json',
            async: false,
            type: 'GET',
            success: function (postTypes) {
                $scope.postTypes = postTypes;
            },
            error: function (data) {
                alert(data.message);
            }
    });

    $scope.save = function () {
        $scope.post.change_date = Date.now();
        $scope.post.tags.forEach(function (tag) {
            if (!('id' in tag)) {
                tag.id = null;
            };
        });
        $http.post("/app/rest/post_page/", $scope.post)
            .success(function () {
                $('#savePostModal').modal('hide');
                window.location.reload();
            })
            .error(function (data) {
                alert(data.message);
            });
    };

    $scope.update = function (id) {
        $('#editPostModal').modal('show');
        $scope.id = id;
    };

    $scope.delete = function (id) {
        $http.delete("/app/rest/post_page/" + $routeParams.id)
            .success(function () {
                window.location = $scope.TECH_POSTS_URL;
            })
            .error(function (data) {
                alert(data.message);
            });
    };

    $scope.addComment = function () {
        $scope.newcomment.create_date = Date.now();
        $scope.newcomment.change_date = Date.now();
        $scope.newcomment.post = $scope.post;
        $http.post("/app/rest/post_page/add_comment", $scope.newcomment)
            .success(function () {
                window.location.reload();
                $scope.newcomment = null;
            })
            .error(function (data) {
                alert(data.message);
            });
    };

    $scope.update_comment = function (com) {
        var updatedComment = new Object();
        updatedComment.id = com.id;
        updatedComment.text = com.text;
        updatedComment.author = com.author;
        updatedComment.create_date = com.create_date;
        updatedComment.change_date = Date.now();
        updatedComment.post = $scope.post;
        $http.post("/app/rest/post_page/update_comment", updatedComment)
            .success(function () {
                window.location.reload();
            })
            .error(function (data) {
                alert(data.message);
            });
    }

    $scope.delete_comment = function (id) {
        $http.delete("/app/rest/post_page/delete_comment/" + id)
            .success(function () {
                window.location.reload();
            })
            .error(function (data) {
                alert(data.message);
            });
    }

    $scope.addSubcomment = function (subcommemt, com) {
        var emptyComment = new Object();
        emptyComment.id = com.id;
        subcommemt.comment = emptyComment;
        subcommemt.create_date = Date.now();
        subcommemt.change_date = Date.now();

        $http.post("/app/rest/post_page/add_subComment", subcommemt)
            .success(function () {
                window.location.reload();
            })
            .error(function (data) {
                alert(data.message);
            });
    }
    $scope.delete_subcomment = function (id) {
        $http.delete("/app/rest/post_page/delete_subcomment/" + id)
            .success(function () {
                window.location.reload();
            })
            .error(function (data, status, headers, config) {
                alert(data + "  " + status);
            });
    }

    $scope.update_subcomment = function (subcom, com) {
        var updatedSubcomment = new Object();
        var emptyComment = new Object();
        emptyComment.id = com.id;
        updatedSubcomment.id = subcom.id;
        updatedSubcomment.text = subcom.text;
        updatedSubcomment.author = subcom.author;
        updatedSubcomment.create_date = subcom.create_date;
        updatedSubcomment.change_date = Date.now();
        updatedSubcomment.comment = emptyComment;
        $http.post("/app/rest/post_page/update_subcomment", updatedSubcomment)
            .success(function () {
                window.location.reload();
            })
            .error(function (data) {
                alert(data.message);
            });
    }

    //        tagsInput
   $scope.loadTags = function(query){
           return  LoadTagsService.getAllTags(query);
   };

});
