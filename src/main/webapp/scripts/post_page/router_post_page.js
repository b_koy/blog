'use strict';
blogApp
    .config(function ($routeProvider, $httpProvider, $translateProvider, USER_ROLES) {
            $routeProvider
                .when('/post_page/:id', {
                    templateUrl: 'views/post_page.html',
                    controller: 'PostPageController',
                    access: {
                        authorizedRoles: [USER_ROLES.all]
                    }
                })
        });