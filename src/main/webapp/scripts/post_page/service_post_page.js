'use strict';

blogApp.factory('Post', function ($resource) {
        return $resource('app/rest/post_page/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': { method: 'GET'}
        });
    });
