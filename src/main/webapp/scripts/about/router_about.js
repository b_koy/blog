'use strict';

blogApp
    .config(function ($routeProvider, $httpProvider, $translateProvider, USER_ROLES) {
        $routeProvider
            .when('/about', {
                templateUrl: 'views/about.html',
                controller: 'AboutController',
                access: {
                    authorizedRoles: [USER_ROLES.all]
                }
            })

    });

