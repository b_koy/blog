'use strict';

blogApp.controller('AboutController', function ($scope, $http) {
     //Set SEO from resources/seo.properties
    $http.get('resources/seo.properties').then(function (response) {
        angular.element($("[name='description']")).attr('content', response.data.AboutControllerSeoDescription);
        angular.element($("[name='keywords']")).attr('content',response.data.AboutControllerSeoKeywords);
    });

    $.ajax({
            url: "/app/rest/about",
            async: false,
            type: 'GET',
            success: function (text) {
                $scope.about = text;
            },
            error: function (data) {
                alert("Error: "+ data.message);
            }
    });

    $scope.update = function (id) {
            $('#editAboutModal').modal('show');
    };

    $scope.save = function () {
            $http.post("/app/rest/aboutUpdate", $scope.about)
                .success(function () {
                    $('#savePostModal').modal('hide');
                    window.location.reload();
                })
                .error(function (data) {
                    alert(data.message);
                });
    };
});
