'use strict';

blogApp
    .config(function ($routeProvider, $httpProvider, $translateProvider, USER_ROLES) {
        $routeProvider
            .when('/technical_posts', {
                templateUrl: 'views/tech_posts.html',
                controller: 'TechPostController',
                resolve: {
                    resolvedPost: ['TechnicalPostFactory', function (TechPostFactory) {
                        return TechPostFactory.query();
                    }]
                },
                access: {
                    authorizedRoles: [USER_ROLES.all]
                }
            })
            .when('/personal_posts', {
                templateUrl: 'views/tech_posts.html',
                controller: 'TechPostController',
                resolve: {
                     resolvedPost: ['PersonalPostFactory', function (TechPostFactory) {
                         return TechPostFactory.query();
                     }]
                },
                access: {
                     authorizedRoles: [USER_ROLES.all]
                }
            })
            .when('/archive', {
                 templateUrl: 'views/archive.html',
                 controller: 'TechPostController',
                 resolve: {
                      resolvedPost: ['ArchivePostFactory', function (TechPostFactory) {
                          return TechPostFactory.query();
                      }]
                 },
                 access: {
                      authorizedRoles: [USER_ROLES.all]
                 }
            })
            .when('/tech_posts_with_tag/:tagId', {
                templateUrl: 'views/tech_posts.html',
                controller: 'TechPostByTagController',
                access: {
                    authorizedRoles: [USER_ROLES.all]
                }
            })

    });

