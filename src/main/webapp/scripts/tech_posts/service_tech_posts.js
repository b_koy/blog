'use strict';

blogApp.factory('TechPostFactory', function ($resource) {
    return $resource('app/rest/tech_posts/:id', {}, {
        'query': {method: 'GET', isArray: true},
        'get': {method: 'GET'}
    });
});

blogApp.factory('TechnicalPostFactory', function ($resource) {
    return $resource('app/rest/technical_posts/:id', {}, {
        'query': {method: 'GET', isArray: true},
        'get': {method: 'GET'}
    });
});
blogApp.factory('PersonalPostFactory', function ($resource) {
    return $resource('app/rest/personal_posts/:id', {}, {
        'query': {method: 'GET', isArray: true},
        'get': {method: 'GET'}
    });
});
blogApp.factory('ArchivePostFactory', function ($resource) {
    return $resource('app/rest/archive/:id', {}, {
        'query': {method: 'GET', isArray: true},
        'get': {method: 'GET'}
    });
});
// Load all tags
blogApp.factory('LoadTagsService', function ($q) {
    var root = {};
            root.getAllTags = function(query){
               var test;
                       var defer = $q.defer();
                       $.ajax({
                           url: "/app/rest/tech_posts/getTags?query=" + query,
                           dataType: 'json',
                           async: false,
                           type: 'GET',
                           success: function (tags) {
                               test = tags;
                           },
                           error: function (data) {
                               alert(data.message);
                           }
                       });
                       defer.resolve(test);
                       return defer.promise;
            };
            return root;
});
