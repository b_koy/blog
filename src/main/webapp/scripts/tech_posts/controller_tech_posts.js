'use strict';
var ITEMS_ON_PAGE = 5;
var FIRST_PAGE = 0;
blogApp.controller('TechPostController', function ($scope, resolvedPost, TechPostFactory,LoadTagsService, $http, $q, Tag) {
     //Set SEO from resources/seo.properties
    $http.get('resources/seo.properties').then(function (response) {
        angular.element($("[name='description']")).attr('content', response.data.TechPostControllerSeoDescription);
        angular.element($("[name='keywords']")).attr('content',response.data.TechPostControllerSeoKeywords);
    });

//        pagination
    $scope.itemsPerPage = ITEMS_ON_PAGE;
    $scope.currentPage = FIRST_PAGE;
    $scope.items = resolvedPost;

    $scope.range = function () {
        var rangeSize = Math.ceil($scope.items.length / $scope.itemsPerPage);
        var ret = [];
        var start;

        start = $scope.currentPage;
        if (start > $scope.pageCount() - rangeSize) {
            start = $scope.pageCount() - rangeSize + 1;
        }

        for (var i = start; i < start + rangeSize; i++) {
            ret.push(i);
        }
        return ret;
    };

    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
            $scope.currentPage--;
        }
    };

    $scope.prevPageDisabled = function () {
        return $scope.currentPage === 0 ? "disabled" : "";
    };

    $scope.pageCount = function () {
        return Math.ceil($scope.items.length / $scope.itemsPerPage) - 1;
    };

    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pageCount()) {
            $scope.currentPage++;
        }
    };

    $scope.nextPageDisabled = function () {
        return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
    };

    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };

});

//    filter for pagination
blogApp.filter('offset', function () {
    return function (input, start) {
        start = parseInt(start, 10);
        return input.slice(start);
    };
});

blogApp.controller('TechPostByTagController', function ($scope, $http, $q, $routeParams, LoadTagsService) {
     //Set SEO from resources/seo.properties
    $http.get('resources/seo.properties').then(function (response) {
        angular.element($("[name='description']")).attr('content', response.data.TechPostByTagControllerSeoDescription);
        angular.element($("[name='keywords']")).attr('content',response.data.TechPostByTagControllerSeoKeywords);
    });

    $.ajax({
        url: "/app/rest/tech_posts_with_tag/" + $routeParams.tagId,
        dataType: 'json',
        async: false,
        type: 'GET',
        success: function (posts) {
            $scope.items = posts;
        },
        error: function (data) {
            alert(data.message);
        }
    });

    $scope.create = function () {
        $scope.post.create_date = Date.now();
        $scope.post.change_date = Date.now();
        $scope.post.tags.forEach(function (tag) {
            if (!('id' in tag)) {
                tag.id = null;
            }
        });
        $http.post("/app/rest/post_page/", $scope.post)
            .success(function () {
                $('#savePostModal').modal('hide');
                window.location.reload();
            })
            .error(function (data) {
                alert(data.message);
            });
    };

    $scope.clear = function () {
        $scope.TechPostFactory = {title: null, text: null, create_date: null, change_date: null, id: null};
    };

//        pagination
    $scope.itemsPerPage = ITEMS_ON_PAGE;
    $scope.currentPage = FIRST_PAGE;

    $scope.range = function () {
        var rangeSize = Math.ceil($scope.items.length / $scope.itemsPerPage);
        var ret = [];
        var start;

        start = $scope.currentPage;
        if (start > $scope.pageCount() - rangeSize) {
            start = $scope.pageCount() - rangeSize + 1;
        }

        for (var i = start; i < start + rangeSize; i++) {
            ret.push(i);
        }
        return ret;
    };

    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
            $scope.currentPage--;
        }
    };

    $scope.prevPageDisabled = function () {
        return $scope.currentPage === 0 ? "disabled" : "";
    };

    $scope.pageCount = function () {
        return Math.ceil($scope.items.length / $scope.itemsPerPage) - 1;
    };

    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pageCount()) {
            $scope.currentPage++;
        }
    };

    $scope.nextPageDisabled = function () {
        return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
    };

    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };

//        tagsInput
    $scope.loadTags = function(query){
            return  LoadTagsService.getAllTags(query);
    };
});

blogApp.controller('TechPostSearchController', function ($scope, $routeParams, $http, $q, LoadTagsService) {
     //Set SEO from resources/seo.properties
    $http.get('resources/seo.properties').then(function (response) {
        angular.element($("[name='description']")).attr('content', response.data.TechPostSearchControllerSeoDescription);
        angular.element($("[name='keywords']")).attr('content',response.data.TechPostSearchControllerSeoKeywords);
    });

    $.ajax({
        url: "/app/rest/tech_posts/search?tagsNames=" + $routeParams.tagsNames,
        dataType: 'json',
        async: false,
        type: 'GET',
        success: function (posts) {
            $scope.items = posts;
        }
    });

    $scope.create = function () {
        $scope.post.create_date = Date.now();
        $scope.post.change_date = Date.now();
        $scope.post.tags.forEach(function (tag) {
            if (!('id' in tag)) {
                tag.id = null;
            }
        });
        TechPostFactory.save($scope.post,
            function () {
                $scope.posts = TechPostFactory.query();
                $('#savePostModal').modal('hide');
                $scope.clear();
                window.location.reload();
            });
    };

    $scope.clear = function () {
        $scope.TechPostFactory = {title: null, text: null, create_date: null, change_date: null, id: null};
    };

//        pagination
    $scope.itemsPerPage = ITEMS_ON_PAGE;
    $scope.currentPage = FIRST_PAGE;

    $scope.range = function () {
        var rangeSize = Math.ceil($scope.items.length / $scope.itemsPerPage);
        var ret = [];
        var start;

        start = $scope.currentPage;
        if (start > $scope.pageCount() - rangeSize) {
            start = $scope.pageCount() - rangeSize + 1;
        }

        for (var i = start; i < start + rangeSize; i++) {
            ret.push(i);
        }
        return ret;
    };

    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
            $scope.currentPage--;
        }
    };

    $scope.prevPageDisabled = function () {
        return $scope.currentPage === 0 ? "disabled" : "";
    };

    $scope.pageCount = function () {
        return Math.ceil($scope.items.length / $scope.itemsPerPage) - 1;
    };

    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pageCount()) {
            $scope.currentPage++;
        }
    };

    $scope.nextPageDisabled = function () {
        return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
    };

    $scope.setPage = function (n) {
        $scope.currentPage = n;
    };

//        tagsInput
    $scope.loadTags = function(query){
            return  LoadTagsService.getAllTags(query);
    };
});
