/**
 * Created by Bohdan Kolesnyk on 1/8/2015.
 */
blogApp.controller('SearchController', function ($scope, $location, $route) {
    $scope.allTags =[];
    $scope.selectedTags = [];
    $.ajax({
        url: "/app/rest/tech_posts/getAllRelatedTags?query=",
        dataType: 'json',
        async: false,
        type: 'GET',
        success: function (tags) {
            $scope.allTags = [];
            tags.forEach(function(tag){
                var obj = new Object();
                obj.active = false;
                obj.text = tag.text
                $scope.allTags.push(obj);
            });
        },
        error: function (data) {
            alert(data.message);
        }
    });

    //$('#langtabs').tabSelect({
    //    tabElements: $scope.allTags,
    //    selectedTabs: $scope.selectedTags
    //});
    $scope.getRelatedTags = function () {
        $scope.selectedTags = $scope.getAllSelected()
        $.ajax({
            url: "/app/rest/tech_posts/getAllRelatedTags?query=" + $scope.selectedTags,
            dataType: 'json',
            async: false,
            type: 'GET',
            success: function (tags) {
                var arr = tags;
                arr.forEach(function (tag) {
                    $scope.selectedTags.forEach(function (selectedTag) {
                        if (tag.text === selectedTag) {
                            tag.active = true;
                        }
                    });
                });
                $scope.allTags = arr;
            },
            error: function (data) {
                alert(data.message);
            }
        });
    };

    $scope.search = function(){
        var tagsNames = [];
        $('#langtabs').find('.active').each(function(element){
            tagsNames.push($(this).text());
        });
        var t = $scope.getAllSelected();
        if(t.length === 0){
            t[0] = "all";
        };
        $location.path("/tech_posts/search/" +  t);
        //$('#searchButton').click();
    }

    $scope.getAllSelected = function() {
        var tagsNames = [];
        $scope.allTags.forEach(function(tag){
            if (tag.active === true){
                tagsNames.push(tag.text);
            }
        })
        return tagsNames;
    }
});
