/**
 * Created by Bohdan Kolesnyk on 1/8/2015.
 */
'use strict';
blogApp
    .config(function ($routeProvider, $httpProvider, $translateProvider, USER_ROLES) {
        $routeProvider
            .when('/tech_posts/search/:tagsNames', {
                templateUrl: 'views/tech_posts.html',
                controller: 'TechPostSearchController',
                access: {
                    authorizedRoles: [USER_ROLES.all]
                }
            })

    });
