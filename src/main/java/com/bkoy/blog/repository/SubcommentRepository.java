package com.bkoy.blog.repository;

import com.bkoy.blog.domain.Subcomment;
        import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the Subcomment entity.
 */
public interface SubcommentRepository extends JpaRepository<Subcomment, Long> {

}
