package com.bkoy.blog.repository;

import com.bkoy.blog.domain.Post;
import com.bkoy.blog.domain.Tag;
import com.bkoy.blog.domain.util.PostType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Post entity.
 */
public interface PostRepository extends JpaRepository<Post, Long> {

    @Query("SELECT p FROM Post p WHERE p.postType = :postType)")
    public List<Post> findByType(@Param("postType") PostType postType);
}
