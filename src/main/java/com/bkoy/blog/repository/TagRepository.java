package com.bkoy.blog.repository;

import com.bkoy.blog.domain.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Set;

/**
 * Spring Data JPA repository for the Tag entity.
 */
public interface TagRepository extends JpaRepository<Tag, Long> {

    @Query("select tag from Tag tag left join fetch tag.posts where tag.id = :id")
    Tag findOneWithEagerRelationships(@Param("id") Long id);

    @Query("select tag from Tag tag where tag.text = :text")
    Tag findOneByText(@Param("text") String text);

    @Query("select tag from Tag tag where tag.text like :text")
    Set<Tag> findAllWithText(@Param("text") String text);

}
