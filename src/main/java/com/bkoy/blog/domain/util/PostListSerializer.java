package com.bkoy.blog.domain.util;

import com.bkoy.blog.domain.Comment;
import com.bkoy.blog.domain.Post;
import com.bkoy.blog.domain.Subcomment;
import com.bkoy.blog.domain.Tag;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.Set;

/**
 * Created by bkoles on 30.12.2014.
 */
public class PostListSerializer extends JsonSerializer<Set<Post>> {
    @Override
    public void serialize(Set<Post> posts, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeStartArray();
        for(Post post : posts){
            jsonGenerator.writeStartObject();
            jsonGenerator.writeObjectField("id", post.getId());
            jsonGenerator.writeObjectField("title", post.getTitle());
            jsonGenerator.writeObjectField("text", post.getText());
            jsonGenerator.writeObjectField("postType", post.getPostType());
            jsonGenerator.writeObjectField("seoDescription", post.getSeoDescription());
            jsonGenerator.writeObjectField("seoKeywords", post.getSeoKeywords());
            jsonGenerator.writeObjectField("create_date", post.getCreate_date());
            jsonGenerator.writeObjectField("change_date", post.getChange_date());
            jsonGenerator.writeArrayFieldStart("comments");
            for (Comment comment : post.getComments()) {
                jsonGenerator.writeStartObject();
                jsonGenerator.writeObjectField("id", comment.getId());
                jsonGenerator.writeObjectField("text", comment.getText());
                jsonGenerator.writeObjectField("author", comment.getAuthor());
                jsonGenerator.writeObjectField("create_date", comment.getCreate_date());
                jsonGenerator.writeObjectField("change_date", comment.getChange_date());
                jsonGenerator.writeArrayFieldStart("subcomments");
                for (Subcomment subcomment : comment.getSubcomments()) {
                    jsonGenerator.writeStartObject();
                    jsonGenerator.writeObjectField("id", subcomment.getId());
                    jsonGenerator.writeObjectField("text", subcomment.getText());
                    jsonGenerator.writeObjectField("author", subcomment.getAuthor());
                    jsonGenerator.writeObjectField("create_date", subcomment.getCreate_date());
                    jsonGenerator.writeObjectField("change_date", subcomment.getChange_date());
                    jsonGenerator.writeObjectField("comment_id", subcomment.getComment().getId());
                    jsonGenerator.writeEndObject();
                }
                jsonGenerator.writeEndArray();
                jsonGenerator.writeEndObject();
            }
            jsonGenerator.writeEndArray();
            jsonGenerator.writeArrayFieldStart("tags");
            for (Tag tag : post.getTags()) {
                jsonGenerator.writeStartObject();
                jsonGenerator.writeObjectField("id", tag.getId());
            }
            jsonGenerator.writeEndArray();
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();
    }
}
