package com.bkoy.blog.domain.util;

import com.bkoy.blog.domain.Subcomment;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.Set;

/**
 * Subcomment list serializer
 * Created by bkoles on 03.12.2014.
 */
public class SubcommentListSerializer extends JsonSerializer<Set<Subcomment>> {
    @Override
    public void serialize(Set<Subcomment> subcomments, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartArray();
        for (Subcomment subcomment : subcomments) {
            jsonGenerator.writeStartObject();
            jsonGenerator.writeObjectField("id", subcomment.getId());
            jsonGenerator.writeObjectField("text", subcomment.getText());
            jsonGenerator.writeObjectField("author", subcomment.getAuthor());
            jsonGenerator.writeObjectField("create_date", subcomment.getCreate_date());
            jsonGenerator.writeObjectField("change_date", subcomment.getChange_date());
            jsonGenerator.writeObjectField("comment_id", subcomment.getComment().getId());
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();
    }
}
