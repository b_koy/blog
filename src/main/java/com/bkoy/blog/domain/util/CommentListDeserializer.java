package com.bkoy.blog.domain.util;

import com.bkoy.blog.domain.Comment;
import com.bkoy.blog.domain.Post;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Comment list Deserializer
 * Created by Bohdan on 27.10.2014.
 */
public class CommentListDeserializer extends JsonDeserializer<Set<Comment>> {
    private static DateTimeFormatter formatter =
            DateTimeFormat.forPattern("[yyyy,MM,dd]");
    @Override
    public Set<Comment> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        TreeNode commentTreeNodeList = jsonParser.readValueAsTree();
        Set<Comment> resultSet = new LinkedHashSet<>();
        for(int i = 0; i < commentTreeNodeList.size(); i++){
            Comment comment = new Comment();
            TreeNode commentTreeNode = commentTreeNodeList.get(i);
            comment.setId(Long.parseLong(commentTreeNode.get("id").toString()));
            comment.setText(commentTreeNode.get("text").toString().replaceAll("\"", ""));
            comment.setCreate_date(formatter.parseLocalDate(commentTreeNode.get("create_date").toString()));
            comment.setChange_date(formatter.parseLocalDate(commentTreeNode.get("change_date").toString()));
            comment.setAuthor(commentTreeNode.get("author").toString().replaceAll("\"", ""));
            Post post = new Post();
            post.setId(Long.parseLong(commentTreeNode.get("post_id").toString()));
            comment.setPost(post);
            resultSet.add(comment);
        }
        return resultSet;
    }
}


