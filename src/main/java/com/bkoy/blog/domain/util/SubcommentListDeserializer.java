package com.bkoy.blog.domain.util;

import com.bkoy.blog.domain.Comment;
import com.bkoy.blog.domain.Post;
import com.bkoy.blog.domain.Subcomment;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Subcomment list deserializer
 * Created by bkoles on 03.12.2014.
 */
public class SubcommentListDeserializer extends JsonDeserializer<Set<Subcomment>> {
    private static DateTimeFormatter formatter =
        DateTimeFormat.forPattern("[yyyy,MM,dd]");

    @Override
    public Set<Subcomment> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        TreeNode subcommentTreeNodeList = jsonParser.readValueAsTree();
        Set<Subcomment> resultSet = new LinkedHashSet<>();
        for (int i = 0; i < subcommentTreeNodeList.size(); i++) {
            Subcomment subcomment = new Subcomment();
            TreeNode subcommentTreeNode = subcommentTreeNodeList.get(i);
            subcomment.setId(Long.parseLong(subcommentTreeNode.get("id").toString()));
            subcomment.setText(subcommentTreeNode.get("text").toString().replaceAll("\"", ""));
            subcomment.setCreate_date(formatter.parseLocalDate(subcommentTreeNode.get("create_date").toString()));
            subcomment.setChange_date(formatter.parseLocalDate(subcommentTreeNode.get("change_date").toString()));
            subcomment.setAuthor(subcommentTreeNode.get("author").toString().replaceAll("\"", ""));
            Comment comment = new Comment();
            comment.setId(Long.parseLong(subcommentTreeNode.get("comment_id").toString()));
            subcomment.setComment(comment);
            resultSet.add(subcomment);
        }
        return resultSet;
    }
}
