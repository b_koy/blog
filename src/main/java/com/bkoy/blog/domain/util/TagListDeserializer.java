package com.bkoy.blog.domain.util;

import com.bkoy.blog.domain.Tag;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;


/**
 * Tag list Deserializer
 * Created by bkoles on 29.12.2014.
 */
public class TagListDeserializer extends JsonDeserializer<Set<Tag>> {
    @Override
    public Set<Tag> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        TreeNode commentTreeNodeList = jsonParser.readValueAsTree();
        Set<Tag> resultSet = new LinkedHashSet<>();
        for (int i = 0; i < commentTreeNodeList.size(); i++) {
            Tag tag = new Tag();
            TreeNode commentTreeNode = commentTreeNodeList.get(i);
            TreeNode tmp = commentTreeNode.get("id");
            // for correct serialize
            if (!tmp.toString().equals("null")) {
                tag.setId(Long.parseLong(commentTreeNode.get("id").toString()));
            } else {
                //this id didn't used
                tag.setId((long) (i + 1000));
            }
            tag.setText(commentTreeNode.get("text").toString().substring(1, commentTreeNode.get("text").toString().length() - 1));
//                TreeNode postsTreeNodeList = commentTreeNode.get("posts");
//                Set<Post> postList = new LinkedHashSet<>();
//                for(int j = 0; j <postsTreeNodeList.size(); j++){
//                    Post post = new Post();
//                    TreeNode postTreeNode = postsTreeNodeList.get(j);
//                    post.setId(Long.parseLong(postTreeNode.get("post_id").toString()));
//                    postList.add(post);
//                }
//                tag.setPosts(postList);
            resultSet.add(tag);
        }
        return resultSet;
    }
}
