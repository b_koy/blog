package com.bkoy.blog.domain.util;

import com.bkoy.blog.domain.Comment;
import com.bkoy.blog.domain.Post;
import com.bkoy.blog.domain.Subcomment;
import com.bkoy.blog.domain.Tag;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.Set;

/**
 * Tag list Serializer
 * Created by bkoles on 29.12.2014.
 */
public class TagListSerializer extends JsonSerializer<Set<Tag>> {
    @Override
    public void serialize(Set<Tag> tags, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeStartArray();
        for(Tag tag : tags){
            jsonGenerator.writeStartObject();
            jsonGenerator.writeObjectField("id", tag.getId());
            jsonGenerator.writeObjectField("text", tag.getText());
//            jsonGenerator.writeArrayFieldStart("posts");
//            for (Post post : tag.getPosts()) {
//                jsonGenerator.writeStartObject();
//                jsonGenerator.writeObjectField("id", post.getId());
//                jsonGenerator.writeEndObject();
//            }
//            jsonGenerator.writeEndArray();
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();
    }
}
