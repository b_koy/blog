package com.bkoy.blog.domain.util;

import com.bkoy.blog.domain.Comment;
import com.bkoy.blog.domain.Post;
import com.bkoy.blog.domain.Tag;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Post List Deserializer
 * Created by bkoles on 30.12.2014.
 */
public class PostListDeserializer extends JsonDeserializer<Set<Post>> {
    private static DateTimeFormatter formatter =
        DateTimeFormat.forPattern("[yyyy-MM-dd HH:mm:ss]");
    @Override
    public Set<Post> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        TreeNode commentTreeNodeList = jsonParser.readValueAsTree();
        Set<Post> resultSet = new LinkedHashSet<>();
        for(int i = 0; i < commentTreeNodeList.size(); i++){
            Post post = new Post();
            TreeNode postTreeNode = commentTreeNodeList.get(i);
            post.setId(Long.parseLong(postTreeNode.get("id").toString()));
            post.setTitle(postTreeNode.get("title").toString());
            post.setText(postTreeNode.get("text").toString().replaceAll("\"", ""));
//            post.setPostType(PostType.getTypeByName(postTreeNode.get("postType").get("typeName").toString()));
            post.setSeoDescription(postTreeNode.get("seoDescription").toString());
            post.setSeoKeywords(postTreeNode.get("seoKeywords").toString());
            post.setCreate_date(formatter.parseLocalDate(postTreeNode.get("create_date").toString()));
            post.setChange_date(formatter.parseLocalDate(postTreeNode.get("change_date").toString()));
            TreeNode commentsTreeNodeList = postTreeNode.get("comments");
            Set<Comment> comments = new LinkedHashSet<>();
            for(int j = 0; j <commentsTreeNodeList.size(); j++){
                Comment comment = new Comment();
                TreeNode commentTreeNode = commentsTreeNodeList.get(j);
                post.setId(Long.parseLong(commentTreeNode.get("tag_id").toString()));
                comments.add(comment);
            }
            post.setComments(comments);
            TreeNode tagsTreeNodeList = postTreeNode.get("tags");
            Set<Tag> tags = new LinkedHashSet<>();
            for(int j = 0; j <tagsTreeNodeList.size(); j++){
                Tag tag = new Tag();
                TreeNode tagTreeNode = tagsTreeNodeList.get(j);
                post.setId(Long.parseLong(tagTreeNode.get("tag_id").toString()));
                tags.add(tag);
            }
            post.setTags(tags);
            resultSet.add(post);
        }
        return resultSet;
    }
}
