package com.bkoy.blog.domain.util;

import com.bkoy.blog.domain.Comment;
import com.bkoy.blog.domain.Subcomment;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Set;

/**
 * Comment list serializer
 * Created by Bohdan on 27.10.2014.
 */
public class CommentListSerializer extends JsonSerializer<Set<Comment>> {
    @Override
    public void serialize(Set<Comment> comments, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeStartArray();
        for(Comment comment : comments){
            jsonGenerator.writeStartObject();
            jsonGenerator.writeObjectField("id", comment.getId());
            jsonGenerator.writeObjectField("text", comment.getText());
            jsonGenerator.writeObjectField("author", comment.getAuthor());
            jsonGenerator.writeObjectField("create_date", comment.getCreate_date());
            jsonGenerator.writeObjectField("change_date", comment.getChange_date());
            jsonGenerator.writeObjectField("post_id", comment.getPost().getId());
            jsonGenerator.writeArrayFieldStart("subcomments");
            for (Subcomment subcomment : comment.getSubcomments()) {
                jsonGenerator.writeStartObject();
                jsonGenerator.writeObjectField("id", subcomment.getId());
                jsonGenerator.writeObjectField("text", subcomment.getText());
                jsonGenerator.writeObjectField("author", subcomment.getAuthor());
                jsonGenerator.writeObjectField("create_date", subcomment.getCreate_date());
                jsonGenerator.writeObjectField("change_date", subcomment.getChange_date());
                jsonGenerator.writeObjectField("comment_id", subcomment.getComment().getId());
                jsonGenerator.writeEndObject();
            }
            jsonGenerator.writeEndArray();
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();
    }
}
