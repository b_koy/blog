package com.bkoy.blog.domain.util;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import javafx.geometry.Pos;

import java.io.Serializable;

/**
 * Post tyepe enum
 * Created by Bohdan Kolesnyk on 7/18/2015.
 */
public enum PostType {
    TECHNICAL("technical", 0),
    PERSONAL("personal", 1);
    private String typeName;
    private int id;

    PostType(String typeName, int id){
        this.typeName = typeName;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getTypeName() {
        return typeName;
    }

    public static PostType getTypeByName(String typeName){
        for(PostType v : values()){
            if( v.getTypeName().equals(typeName)){
                return v;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "PostType{" +
            "typeName='" + typeName + '\'' +
            ", id=" + id +
            '}';
    }

}
