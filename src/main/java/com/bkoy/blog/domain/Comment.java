package com.bkoy.blog.domain;

import com.bkoy.blog.domain.util.CustomLocalDateSerializer;
import com.bkoy.blog.domain.util.SubcommentListDeserializer;
import com.bkoy.blog.domain.util.SubcommentListSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.joda.deser.LocalDateDeserializer;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * A Comment.
 */
@Entity
@Table(name = "T_COMMENT")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Comment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Column(name = "text")
    private String text;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = CustomLocalDateSerializer.class)
    @Column(name = "create_date", nullable = false)
    private LocalDate create_date;

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = CustomLocalDateSerializer.class)
    @Column(name = "change_date", nullable = false)
    private LocalDate change_date;

    @Column(name = "author")
    private String author;

    @ManyToOne
    private Post post;

    @OneToMany(orphanRemoval = true, fetch = FetchType.EAGER, mappedBy = "comment")
    @JsonDeserialize(using = SubcommentListDeserializer.class)
    @JsonSerialize(using = SubcommentListSerializer.class)
//    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Subcomment> subcomments;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDate getCreate_date() {
        return create_date;
    }

    public void setCreate_date(LocalDate create_date) {
        this.create_date = create_date;
    }

    public LocalDate getChange_date() {
        return change_date;
    }

    public void setChange_date(LocalDate change_date) {
        this.change_date = change_date;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public Set<Subcomment> getSubcomments() {
        return subcomments;
    }

    public void setSubcomments(Set<Subcomment> subcomments) {
        this.subcomments = subcomments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Comment comment = (Comment) o;

        if (id != null ? !id.equals(comment.id) : comment.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "Comment{" +
            "id=" + id +
            ", text='" + text + "'" +
            ", create_date='" + create_date + "'" +
            ", change_date='" + change_date + "'" +
            ", author='" + author + "'" +
            '}';
    }
}
