package com.bkoy.blog.web.rest;

import com.bkoy.blog.domain.Comment;
import com.bkoy.blog.domain.Post;
import com.bkoy.blog.domain.Subcomment;
import com.bkoy.blog.repository.CommentRepository;
import com.bkoy.blog.repository.PostRepository;
import com.bkoy.blog.repository.SubcommentRepository;
import com.bkoy.blog.service.PostService;
import com.codahale.metrics.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Rest controller for managing posts for post page
 * @author Bohdan
 *
 */
@RestController
@RequestMapping("/app")
public class PostPageResource {
    private final Logger log = LoggerFactory.getLogger(PostPageResource.class);

    @Inject
    private PostRepository postRepository;

    @Inject
    private CommentRepository commentRepository;

    @Inject
    private SubcommentRepository subcommentRepository;

    @Inject
    private PostService postService;

    /**
     * POST  /rest/post_page -> Create a new tech post.
     */
    @RequestMapping(value = "/rest/post_page",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void create(@RequestBody Post post) {
        log.debug("REST request to save Post : {}", post);
        postService.savePost(post);
    }

    /**
     * GET  /rest/tech_posts -> get all the posts.
     */
    @RequestMapping(value = "/rest/post_page",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Post> getAll() {
        log.debug("REST request to get all Posts");
        return postRepository.findAll();
    }

    /**
     * GET  /rest/post_page/:id -> get the "id" post.
     */
    @RequestMapping(value = "/rest/post_page/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Post> get(@PathVariable Long id, HttpServletResponse response) {
        log.debug("REST request to get Post : {}", id);
        Post post = postRepository.findOne(id);
        if (post == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(post, HttpStatus.OK);
    }

    /**
     * DELETE  /rest/post_page/:id -> delete the "id" post.
     */
    @RequestMapping(value = "/rest/post_page/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Post : {}", id);
        postRepository.delete(id);
    }


    /**
     * POST /rest/post_page/add_comment -> add comment to post.
     */
    @RequestMapping(value = "/rest/post_page/add_comment",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void addComment(@RequestBody Comment comment){
        log.debug("REST request to add comment to post");
        commentRepository.save(comment);
    }

    /**
     * DELETE  /rest/post_page/delete_comment/:id -> delete comment.
     */
    @RequestMapping(value = "/rest/post_page/delete_comment/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void deleteComment(@PathVariable Long id) {
        log.debug("REST request to delete comment with id : {}", id);
        commentRepository.delete(id);
    }

    /**
     * POST  /rest/post_page/update_comment -> update comment.
     */
    @RequestMapping(value = "/rest/post_page/update_comment",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void updateComment(@RequestBody Comment comment) {
        log.debug("REST request to update comment with id : {}", comment.getId());
        Comment existComment = commentRepository.findOne(comment.getId());
        existComment.setAuthor(comment.getAuthor());
        existComment.setText(comment.getText());
        existComment.setChange_date(comment.getChange_date());
        commentRepository.save(existComment);
    }

    /**
     * POST /rest/post_page/add_subComment -> add subcomment to comment.
     */
    @RequestMapping(value = "/rest/post_page/add_subComment",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void addSubcomment(@RequestBody Subcomment subcomment){
        log.debug("REST request to add subcomment to comment");
        subcommentRepository.save(subcomment);
    }

    /**
     * DELETE  /rest/post_page/delete_subcomment/:id -> delete subcomment.
     */
    @RequestMapping(value = "/rest/post_page/delete_subcomment/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void deleteSubcomment(@PathVariable Long id) {
        log.debug("REST request to delete comment with id : {}", id);
        subcommentRepository.delete(id);
    }

    /**
     * POST  /rest/post_page/update_comment -> update subcomment.
     */
    @RequestMapping(value = "/rest/post_page/update_subcomment",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void updateSubcomment(@RequestBody Subcomment subcomment) {
        log.debug("REST request to update subcomment with id : {}", subcomment.getId());
        subcommentRepository.save(subcomment);
    }
}
