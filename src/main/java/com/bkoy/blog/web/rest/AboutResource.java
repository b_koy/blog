package com.bkoy.blog.web.rest;

import com.bkoy.blog.domain.Post;
import com.bkoy.blog.domain.User;
import com.bkoy.blog.repository.UserRepository;
import com.bkoy.blog.security.AuthoritiesConstants;
import com.codahale.metrics.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Bohdan_Kolesnyk on 7/29/2015.
 */
@RestController
@RequestMapping("/app")
public class AboutResource {
    private final Logger log = LoggerFactory.getLogger(AboutResource.class);
    private static final String ADMIN = "admin";

    @Inject
    private UserRepository userRepository;

    /**
     * GET  /rest/users/:login -> get the "login" user.
     */
    @RequestMapping(value = "/rest/about",
        method = RequestMethod.GET,
        produces = MediaType.ALL_VALUE)
    @Timed
    public String getAboutUser(HttpServletResponse response) {
        String result = "";
        log.debug("REST request to get about for User : {}", ADMIN);
        User user = userRepository.findOne(ADMIN);
        if (user == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        } else{
            result = user.getAbout();
        }
        return result;
    }

    /**
     * POST  /rest/post_page -> Create a new tech post.
     */
    @RequestMapping(value = "/rest/aboutUpdate",
        method = RequestMethod.POST,
        produces = MediaType.ALL_VALUE)
    @Timed
    public void update(@RequestBody String text) {
        log.debug("REST request to update About info for User : {}", ADMIN);
        User user = userRepository.findOne(ADMIN);
        user.setAbout(text);
        userRepository.save(user);
    }
}
