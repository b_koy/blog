/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.bkoy.blog.web.rest.dto;
