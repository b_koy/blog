package com.bkoy.blog.web.rest;

import com.bkoy.blog.domain.Post;
import com.bkoy.blog.domain.Tag;
import com.bkoy.blog.domain.util.PostType;
import com.bkoy.blog.repository.PostRepository;
import com.bkoy.blog.repository.TagRepository;
import com.bkoy.blog.service.PostService;
import com.bkoy.blog.service.SearchService;
import com.bkoy.blog.service.TagService;
import com.codahale.metrics.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Set;

/**
 * Rest controller for managing tech posts
 * @author Bohdan
 *
 */
@RestController
@RequestMapping("/app")
public class PostsPageResource {

    private final Logger log = LoggerFactory.getLogger(PostsPageResource.class);

    @Inject
    private PostRepository postRepository;

    @Inject
    private TagRepository tagRepository;

    @Inject
    private PostService postService;

    @Inject
    private TagService tagService;

    @Inject
    private SearchService searchService;

    /**
     * POST  /rest/tech_posts -> Create a new tech post.
     */
    @RequestMapping(value = "/rest/tech_posts",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void create(@RequestBody Post post) {
        log.debug("REST request to save Post : {}", post);
        postService.savePost(post);
    }

    /**
     * GET  /rest/tech_posts -> get all the posts.
     */
    @RequestMapping(value = "/rest/tech_posts",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Post> getAll() {
        log.debug("REST request to get all Posts for TechPost Page");
        return postService.getAllPosts();
    }

    /**
     * GET  /rest/technical_posts -> get all technic posts.
     */
    @RequestMapping(value = "/rest/technical_posts",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Post> getTechPosts() {
        log.debug("REST request to get all Technical Posts");
        return postService.getPostsByType(PostType.TECHNICAL);
    }
    /**
     * GET  /rest/personal_posts -> get all personal posts.
     */
    @RequestMapping(value = "/rest/personal_posts",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Post> getPersonalPosts() {
        log.debug("REST request to get all Personal Posts ");
        return postService.getPostsByType(PostType.PERSONAL);
    }
    /**
     * GET  /rest/archive -> get all archive posts.
     */
    @RequestMapping(value = "/rest/archive",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Post> getPostArchive() {
        log.debug("REST request to get all Archive Posts");
        return postService.getAllPosts();
    }

    /**
     * GET  /rest/getAllPosts -> get all the posts.
     */
    @RequestMapping(value = "/rest/tech_posts/getAllPosts",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Post> getAllPosts() {
        log.debug("REST request to get all Posts");
        return postService.getAllPosts();
    }

    /**
     * GET  /rest/tech_posts/:id -> get the "id" post.
     */
    @RequestMapping(value = "/rest/tech_posts/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Post> get(@PathVariable Long id, HttpServletResponse response) {
        log.debug("REST request to get Post : {}", id);
        Post post = postRepository.findOne(id);
        if (post == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(post, HttpStatus.OK);
    }

    /**
     * DELETE  /rest/tech_posts/:id -> delete the "id" post.
     */
    @RequestMapping(value = "/rest/tech_posts/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Post : {}", id);
        postRepository.delete(id);
    }

    /**
     * GET  /rest/tech_posts/getTags -> all tags.
     */
    @RequestMapping(value = "/rest/tech_posts/getTags",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Set<Tag> getTags(@RequestParam(value = "query") String query) {
        log.debug("Get all tags");
        return tagService.findAllWithText(query);
    }

    /**
     * GET  /rest/tech_posts/:id -> get the "id" post.
     */
    @RequestMapping(value = "/rest/tech_posts_with_tag/{tagId}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Set<Post> getPostsWithTag(@PathVariable Long tagId) {
        log.debug("REST request to get Posts with tag : {}", tagId);
        Tag tag = tagRepository.findOneWithEagerRelationships(tagId);
        return tag.getPosts();
    }

    /**
     * GET  /rest/tech_posts/search -> find posts with tags.
     */
    @RequestMapping(value = "/rest/tech_posts/search",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Post> search(@RequestParam(value = "tagsNames") String tagsNames) {
        log.debug("Search for posts with tags: " + tagsNames);
        return searchService.findPostsByTags(tagsNames);
    }

    /**
     * GET  /rest/tech_posts/getAllRelatedTags -> get all related with posts tags.
     */
    @RequestMapping(value = "/rest/tech_posts/getAllRelatedTags",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Set<Tag> getAllRelatedTags(@RequestParam(value = "query") String query) {
        log.debug("Get all tags");
        return searchService.findAllRealtedTags(query);
    }

    /**
     * GET  /rest/tech_posts/getTagsWithPosts -> all tags which have posts.
     */
    @RequestMapping(value = "/rest/tech_posts/getTagsWithPosts",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public Set<Tag> getTagsWithPosts(@RequestParam(value = "query") String query) {
        log.debug("Get all tags with posts");
        return searchService.findAllRealtedTags(query);
    }

    /**
     * GET  /rest/tech_posts/getPostTypes -> get post types.
     */
    @RequestMapping(value = "/rest/tech_posts/getPostTypes",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public PostType [] getPostTypes() {
        log.debug("REST request to get post types");
        return PostType.values();
    }
}
