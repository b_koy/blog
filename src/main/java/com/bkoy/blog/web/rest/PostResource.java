package com.bkoy.blog.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.bkoy.blog.domain.Post;
import com.bkoy.blog.repository.PostRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * REST controller for managing Post.
 */
@RestController
@RequestMapping("/app")
public class PostResource {

    private final Logger log = LoggerFactory.getLogger(PostResource.class);

    @Inject
    private PostRepository postRepository;

    /**
     * POST  /rest/posts -> Create a new post.
     */
    @RequestMapping(value = "/rest/posts",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void create(@RequestBody Post post) {
        log.debug("REST request to save Post : {}", post);
        postRepository.save(post);
    }

    /**
     * GET  /rest/posts -> get all the posts.
     */
    @RequestMapping(value = "/rest/posts",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Post> getAll() {
        log.debug("REST request to get all Posts");
        return postRepository.findAll();
    }

    /**
     * GET  /rest/posts/:id -> get the "id" post.
     */
    @RequestMapping(value = "/rest/posts/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Post> get(@PathVariable Long id, HttpServletResponse response) {
        log.debug("REST request to get Post : {}", id);
        Post post = postRepository.findOne(id);
        if (post == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(post, HttpStatus.OK);
    }

    /**
     * DELETE  /rest/posts/:id -> delete the "id" post.
     */
    @RequestMapping(value = "/rest/posts/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Post : {}", id);
        postRepository.delete(id);
    }
}
