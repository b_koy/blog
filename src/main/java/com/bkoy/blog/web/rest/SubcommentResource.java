package com.bkoy.blog.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.bkoy.blog.domain.Subcomment;
import com.bkoy.blog.repository.SubcommentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * REST controller for managing Subcomment.
 */
@RestController
@RequestMapping("/app")
public class SubcommentResource {

    private final Logger log = LoggerFactory.getLogger(SubcommentResource.class);

    @Inject
    private SubcommentRepository subcommentRepository;

    /**
     * POST  /rest/subcomments -> Create a new subcomment.
     */
    @RequestMapping(value = "/rest/subcomments",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void create(@RequestBody Subcomment subcomment) {
        log.debug("REST request to save Subcomment : {}", subcomment);
        subcommentRepository.save(subcomment);
    }

    /**
     * GET  /rest/subcomments -> get all the subcomments.
     */
    @RequestMapping(value = "/rest/subcomments",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Subcomment> getAll() {
        log.debug("REST request to get all Subcomments");
        return subcommentRepository.findAll();
    }

    /**
     * GET  /rest/subcomments/:id -> get the "id" subcomment.
     */
    @RequestMapping(value = "/rest/subcomments/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Subcomment> get(@PathVariable Long id, HttpServletResponse response) {
        log.debug("REST request to get Subcomment : {}", id);
        Subcomment subcomment = subcommentRepository.findOne(id);
        if (subcomment == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(subcomment, HttpStatus.OK);
    }

    /**
     * DELETE  /rest/subcomments/:id -> delete the "id" subcomment.
     */
    @RequestMapping(value = "/rest/subcomments/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public void delete(@PathVariable Long id) {
        log.debug("REST request to delete Subcomment : {}", id);
        subcommentRepository.delete(id);
    }
}
