package com.bkoy.blog.service.util;

import com.bkoy.blog.domain.Post;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * Created by bkoles on 04.11.2014.
 */
public class PostTextUtil {
    private static final String P_ATTR = "p";
    private static final String IMG_ATTR = "img";

    /**
     * Cuts text field in each post for posts page
     *
     * @param postList
     * @return
     */
    public static List<Post> cutTextFiled(List<Post> postList) {
        List<Post> results = new ArrayList<>();
        for (Post post : postList) {
            Post newPost = new Post();
            String firstImg = "";
            String firstText = "";

            newPost.setId(post.getId());
            newPost.setChange_date(post.getChange_date());
            newPost.setCreate_date(post.getCreate_date());
            newPost.setComments(post.getComments());
            newPost.setPostType(post.getPostType());
            newPost.setTitle(post.getTitle());
            newPost.setTags(post.getTags());
            newPost.setSeoDescription(post.getSeoDescription());
            newPost.setSeoKeywords(post.getSeoKeywords());

            if (post.getText() != null) {
                Document doc = Jsoup.parse(post.getText());
                firstText= (!doc.select(P_ATTR).isEmpty() ? doc.select(P_ATTR).get(0).toString() : doc.getAllElements().get(0).toString());
                if (!firstText.contains(IMG_ATTR)) {
                    firstImg = (!doc.select(IMG_ATTR).isEmpty() ? doc.select(IMG_ATTR).get(0).toString() : "");
                }
                newPost.setText(firstText + firstImg);
            }

            results.add(newPost);
        }
        return results;
    }
}
