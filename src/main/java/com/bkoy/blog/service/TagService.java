package com.bkoy.blog.service;

import com.bkoy.blog.domain.Tag;
import com.bkoy.blog.repository.TagRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.Set;

/**
 * Tag service
 * @author Bohdan Kolesnyk
 * @since 1/2/2015.
 */

@Service
@Transactional
public class TagService {

    @Inject
    private TagRepository tagRepository;

    /**
     * Find all tags which contains text
     * @param text
     * @return Tag set.
     */
    public Set<Tag> findAllWithText(String text) {
        return tagRepository.findAllWithText(
            new StringBuilder()
                .append("%")
                .append(text)
                .append("%")
                .toString());
    }

}
