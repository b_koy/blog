package com.bkoy.blog.service;

import com.bkoy.blog.domain.Post;
import com.bkoy.blog.domain.Tag;
import com.bkoy.blog.domain.util.PostType;
import com.bkoy.blog.repository.PostRepository;
import com.bkoy.blog.repository.TagRepository;
import com.bkoy.blog.service.util.PostTextUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.*;

/**
 * Service for working with posts
 * Created by bkoles on 02.01.2015.
 */
@Service
@Transactional
public class PostService {

    @Inject
    private PostRepository postRepository;

    @Inject
    private TagRepository tagRepository;

    /**
     * Save post
     * @param post
     */
    public void savePost(Post post) {
        Set<Tag> oldTags = new LinkedHashSet<>();
        Set<Tag> newTags = new LinkedHashSet<>();
        if(post.getId() == null){
            post = postRepository.save(post);
        }else{
            oldTags = postRepository.findOne(post.getId()).getTags();
        }
        for (Tag tag : post.getTags()) {
            Tag newTag = tagRepository.findOneByText(tag.getText());
            if (newTag == null) {
                newTag = new Tag();
                newTag.setText(tag.getText());
                newTag.setPosts(new LinkedHashSet<>(Arrays.asList(post)));
            }else{
                newTag.getPosts().add(post);
            }
            newTags.add(tagRepository.save(newTag));
        }
        post.setTags(newTags);
        removePostFromTag(post, oldTags);
        postRepository.save(post);
    }

    /**
     * Remove post from deleted tag
     * @param post
     * @param oldTags
     */
    private void removePostFromTag(Post post, Set<Tag> oldTags) {
            for (Tag tag : oldTags) {
                if (!post.getTags().contains(tag)) {
                    tag.getPosts().remove(post);
                    tagRepository.save(tag);
                }
            }
    }

    /**
     * Get Tag set for remove many-to-many relationship
     * @param oldTags
     * @param newTags
     * @return
     */
    private Set<Tag> getRemovedTags(Set<Tag> oldTags, Set<Tag> newTags){
        Set<Tag> result = new LinkedHashSet<>();
        for(Tag tag : oldTags){
            if(!newTags.contains(tag)){
                result.add(tag);
            }
        }
        return result;
    }

    /**
     * Get all posts with cutted text field
     * @return
     */
    public List<Post> getAllPosts() {
        List<Post> posts = PostTextUtil.cutTextFiled(postRepository.findAll());
        Collections.sort(posts);
        return posts;
    }

    /**
     * Get post types names
     * @return String array with post type names
     */
    public String [] getPostTypes(){
        String [] postTypes = new String [PostType.values().length];
        int i = 0;
        for(PostType postType : PostType.values()){
            postTypes[i] = postType.getTypeName();
            i++;
        }
        return postTypes;
    }

    /**
     * Get posts list by type
     * @param postType
     * @return List<Post>
     */
    public List<Post> getPostsByType(PostType postType){
        List<Post> posts =  PostTextUtil.cutTextFiled(postRepository.findByType(postType));
        Collections.sort(posts);
        return posts;
    }

}
