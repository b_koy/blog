package com.bkoy.blog.service;

import com.bkoy.blog.domain.Post;
import com.bkoy.blog.domain.Tag;
import com.bkoy.blog.repository.PostRepository;
import com.bkoy.blog.repository.TagRepository;
import com.bkoy.blog.service.util.PostTextUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.*;

/**
 * Search tools service
 * Created by Bohdan Kolesnyk on 1/10/2015.
 */
@Service
@Transactional
public class SearchService {

    @Inject
    private TagRepository tagRepository;

    @Inject
    private PostRepository postRepository;

    @Inject
    private TagService tagService;

    public List<Post> findPostsByTags(String tagsNames){
        String [] tagsNamesArr = tagsNames.split(",");
        List<Post> posts = postRepository.findAll();
        List<Post> resultPosts = new ArrayList<>();
        List<Tag> tags = new ArrayList<>();
        if (tagsNames.equals("all")) {
            return posts;
        } else {
            for (String tagName : tagsNamesArr) {
                Tag tag = tagRepository.findOneByText(tagName);
                tags.add(tag);
            }
        }
        for(Post post : posts){
            if(post.getTags().containsAll(tags)){
                resultPosts.add(post);
            }
        }
        Collections.sort(resultPosts);
        return PostTextUtil.cutTextFiled(resultPosts);
    }

    /**
     * Find all related tags for tagsNames
     * @param tagsNames
     * @return
     */
    public Set<Tag> findAllRealtedTags(String tagsNames){
        Set<Tag> tags = new LinkedHashSet<>();
        if(tagsNames.equals("")){
            tags.addAll(findAllTagsWithPosts(tagService.findAllWithText(tagsNames)));
        }else{
            for(Post post : findPostsByTags(tagsNames)){
                tags.addAll(post.getTags());
            }
        }
        return tags;
    }

    /**
     * Find all tags which have related post
     * @return Tags set
     */
    private Set<Tag> findAllTagsWithPosts(Set<Tag> tags){
        Set<Tag> resultTags = new LinkedHashSet<>();
        for(Tag tag : tags){
            if(findPostsByTags(tag.getText()).size() > 0){
                resultTags.add(tag);
            }
        }
        return resultTags;
    }
}
