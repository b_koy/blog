package com.bkoy.blog.web.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.inject.Inject;
import org.joda.time.LocalDate;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.bkoy.blog.Application;
import com.bkoy.blog.domain.Comment;
import com.bkoy.blog.repository.CommentRepository;

/**
 * Test class for the CommentResource REST controller.
 *
 * @see CommentResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class })
public class CommentResourceTest {
    
    private static final Long DEFAULT_ID = new Long(1L);

    private static final String DEFAULT_TEXT = "SAMPLE_TEXT";
    private static final String UPDATED_TEXT = "UPDATED_TEXT";
        
    private static final LocalDate DEFAULT_CREATE_DATE = new LocalDate(0L);
    private static final LocalDate UPDATED_CREATE_DATE = new LocalDate();
        
    private static final LocalDate DEFAULT_CHANGE_DATE = new LocalDate(0L);
    private static final LocalDate UPDATED_CHANGE_DATE = new LocalDate();
        
    private static final String DEFAULT_AUTHOR = "SAMPLE_TEXT";
    private static final String UPDATED_AUTHOR = "UPDATED_TEXT";
        
    @Inject
    private CommentRepository commentRepository;

    private MockMvc restCommentMockMvc;

    private Comment comment;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CommentResource commentResource = new CommentResource();
        ReflectionTestUtils.setField(commentResource, "commentRepository", commentRepository);

        this.restCommentMockMvc = MockMvcBuilders.standaloneSetup(commentResource).build();

        comment = new Comment();
        comment.setId(DEFAULT_ID);

        comment.setText(DEFAULT_TEXT);
        comment.setCreate_date(DEFAULT_CREATE_DATE);
        comment.setChange_date(DEFAULT_CHANGE_DATE);
        comment.setAuthor(DEFAULT_AUTHOR);
    }

    @Test
    public void testCRUDComment() throws Exception {

        // Create Comment
        restCommentMockMvc.perform(post("/app/rest/comments")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(comment)))
                .andExpect(status().isOk());

        // Read Comment
        restCommentMockMvc.perform(get("/app/rest/comments/{id}", DEFAULT_ID))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(DEFAULT_ID.intValue()))
                .andExpect(jsonPath("$.text").value(DEFAULT_TEXT.toString()))
                .andExpect(jsonPath("$.create_date").value(DEFAULT_CREATE_DATE.toString()))
                .andExpect(jsonPath("$.change_date").value(DEFAULT_CHANGE_DATE.toString()))
                .andExpect(jsonPath("$.author").value(DEFAULT_AUTHOR.toString()));

        // Update Comment
        comment.setText(UPDATED_TEXT);
        comment.setCreate_date(UPDATED_CREATE_DATE);
        comment.setChange_date(UPDATED_CHANGE_DATE);
        comment.setAuthor(UPDATED_AUTHOR);

        restCommentMockMvc.perform(post("/app/rest/comments")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(comment)))
                .andExpect(status().isOk());

        // Read updated Comment
        restCommentMockMvc.perform(get("/app/rest/comments/{id}", DEFAULT_ID))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(DEFAULT_ID.intValue()))
                .andExpect(jsonPath("$.text").value(UPDATED_TEXT.toString()))
                .andExpect(jsonPath("$.create_date").value(UPDATED_CREATE_DATE.toString()))
                .andExpect(jsonPath("$.change_date").value(UPDATED_CHANGE_DATE.toString()))
                .andExpect(jsonPath("$.author").value(UPDATED_AUTHOR.toString()));

        // Delete Comment
        restCommentMockMvc.perform(delete("/app/rest/comments/{id}", DEFAULT_ID)
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Read nonexisting Comment
        restCommentMockMvc.perform(get("/app/rest/comments/{id}", DEFAULT_ID)
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isNotFound());

    }
}
