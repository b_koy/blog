package com.bkoy.blog.web.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.inject.Inject;
import org.joda.time.LocalDate;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.bkoy.blog.Application;
import com.bkoy.blog.domain.Subcomment;
import com.bkoy.blog.repository.SubcommentRepository;

/**
 * Test class for the SubcommentResource REST controller.
 *
 * @see SubcommentResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class })
public class SubcommentResourceTest {
    
    private static final Long DEFAULT_ID = new Long(1L);
    
    private static final String DEFAULT_TEXT = "SAMPLE_TEXT";
    private static final String UPDATED_TEXT = "UPDATED_TEXT";
        
    private static final String DEFAULT_AUTHOR = "SAMPLE_TEXT";
    private static final String UPDATED_AUTHOR = "UPDATED_TEXT";
        
    private static final LocalDate DEFAULT_CREATE_DATE = new LocalDate(0L);
    private static final LocalDate UPDATED_CREATE_DATE = new LocalDate();
        
    private static final LocalDate DEFAULT_CHANGE_DATE = new LocalDate(0L);
    private static final LocalDate UPDATED_CHANGE_DATE = new LocalDate();
        
    @Inject
    private SubcommentRepository subcommentRepository;

    private MockMvc restSubcommentMockMvc;

    private Subcomment subcomment;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SubcommentResource subcommentResource = new SubcommentResource();
        ReflectionTestUtils.setField(subcommentResource, "subcommentRepository", subcommentRepository);

        this.restSubcommentMockMvc = MockMvcBuilders.standaloneSetup(subcommentResource).build();

        subcomment = new Subcomment();
        subcomment.setId(DEFAULT_ID);

        subcomment.setText(DEFAULT_TEXT);
        subcomment.setAuthor(DEFAULT_AUTHOR);
        subcomment.setCreate_date(DEFAULT_CREATE_DATE);
        subcomment.setChange_date(DEFAULT_CHANGE_DATE);
    }

    @Test
    public void testCRUDSubcomment() throws Exception {

        // Create Subcomment
        restSubcommentMockMvc.perform(post("/app/rest/subcomments")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(subcomment)))
                .andExpect(status().isOk());

        // Read Subcomment
        restSubcommentMockMvc.perform(get("/app/rest/subcomments/{id}", DEFAULT_ID))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(DEFAULT_ID.intValue()))
                .andExpect(jsonPath("$.text").value(DEFAULT_TEXT.toString()))
                .andExpect(jsonPath("$.author").value(DEFAULT_AUTHOR.toString()))
                .andExpect(jsonPath("$.create_date").value(DEFAULT_CREATE_DATE.toString()))
                .andExpect(jsonPath("$.change_date").value(DEFAULT_CHANGE_DATE.toString()));

        // Update Subcomment
        subcomment.setText(UPDATED_TEXT);
        subcomment.setAuthor(UPDATED_AUTHOR);
        subcomment.setCreate_date(UPDATED_CREATE_DATE);
        subcomment.setChange_date(UPDATED_CHANGE_DATE);

        restSubcommentMockMvc.perform(post("/app/rest/subcomments")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(subcomment)))
                .andExpect(status().isOk());

        // Read updated Subcomment
        restSubcommentMockMvc.perform(get("/app/rest/subcomments/{id}", DEFAULT_ID))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(DEFAULT_ID.intValue()))
                .andExpect(jsonPath("$.text").value(UPDATED_TEXT.toString()))
                .andExpect(jsonPath("$.author").value(UPDATED_AUTHOR.toString()))
                .andExpect(jsonPath("$.create_date").value(UPDATED_CREATE_DATE.toString()))
                .andExpect(jsonPath("$.change_date").value(UPDATED_CHANGE_DATE.toString()));

        // Delete Subcomment
        restSubcommentMockMvc.perform(delete("/app/rest/subcomments/{id}", DEFAULT_ID)
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Read nonexisting Subcomment
        restSubcommentMockMvc.perform(get("/app/rest/subcomments/{id}", DEFAULT_ID)
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isNotFound());

    }
}
