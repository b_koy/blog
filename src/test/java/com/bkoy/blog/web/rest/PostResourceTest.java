package com.bkoy.blog.web.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.inject.Inject;
import org.joda.time.LocalDate;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.bkoy.blog.Application;
import com.bkoy.blog.domain.Post;
import com.bkoy.blog.repository.PostRepository;

/**
 * Test class for the PostResource REST controller.
 *
 * @see PostResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class })
public class PostResourceTest {
    
    private static final Long DEFAULT_ID = new Long(1L);
    
    private static final String DEFAULT_TITLE = "SAMPLE_TEXT";
    private static final String UPDATED_TITLE = "UPDATED_TEXT";
        
    private static final String DEFAULT_TEXT = "SAMPLE_TEXT";
    private static final String UPDATED_TEXT = "UPDATED_TEXT";
        
    private static final LocalDate DEFAULT_CREATE_DATE = new LocalDate(0L);
    private static final LocalDate UPDATED_CREATE_DATE = new LocalDate();
        
    private static final LocalDate DEFAULT_CHANGE_DATE = new LocalDate(0L);
    private static final LocalDate UPDATED_CHANGE_DATE = new LocalDate();
        
    @Inject
    private PostRepository postRepository;

    private MockMvc restPostMockMvc;

    private Post post;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PostResource postResource = new PostResource();
        ReflectionTestUtils.setField(postResource, "postRepository", postRepository);

        this.restPostMockMvc = MockMvcBuilders.standaloneSetup(postResource).build();

        post = new Post();
        post.setId(DEFAULT_ID);

        post.setTitle(DEFAULT_TITLE);
        post.setText(DEFAULT_TEXT);
        post.setCreate_date(DEFAULT_CREATE_DATE);
        post.setChange_date(DEFAULT_CHANGE_DATE);
    }

    @Test
    public void testCRUDPost() throws Exception {

        // Create Post
        restPostMockMvc.perform(post("/app/rest/posts")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(post)))
                .andExpect(status().isOk());

        // Read Post
        restPostMockMvc.perform(get("/app/rest/posts/{id}", DEFAULT_ID))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(DEFAULT_ID.intValue()))
                .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
                .andExpect(jsonPath("$.text").value(DEFAULT_TEXT.toString()))
                .andExpect(jsonPath("$.create_date").value(DEFAULT_CREATE_DATE.toString()))
                .andExpect(jsonPath("$.change_date").value(DEFAULT_CHANGE_DATE.toString()));

        // Update Post
        post.setTitle(UPDATED_TITLE);
        post.setText(UPDATED_TEXT);
        post.setCreate_date(UPDATED_CREATE_DATE);
        post.setChange_date(UPDATED_CHANGE_DATE);

        restPostMockMvc.perform(post("/app/rest/posts")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(post)))
                .andExpect(status().isOk());

        // Read updated Post
        restPostMockMvc.perform(get("/app/rest/posts/{id}", DEFAULT_ID))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(DEFAULT_ID.intValue()))
                .andExpect(jsonPath("$.title").value(UPDATED_TITLE.toString()))
                .andExpect(jsonPath("$.text").value(UPDATED_TEXT.toString()))
                .andExpect(jsonPath("$.create_date").value(UPDATED_CREATE_DATE.toString()))
                .andExpect(jsonPath("$.change_date").value(UPDATED_CHANGE_DATE.toString()));

        // Delete Post
        restPostMockMvc.perform(delete("/app/rest/posts/{id}", DEFAULT_ID)
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Read nonexisting Post
        restPostMockMvc.perform(get("/app/rest/posts/{id}", DEFAULT_ID)
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isNotFound());

    }
}
